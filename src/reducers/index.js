import { combineReducers } from 'redux';

const todos = (state = [], action) => {
	switch (action.type) {
		case 'ADD_TODO':
			return [...state,
				{
					id: action.id,
					kegiatan: action.text,
					status: false,
				}
			]
		case 'TOGGLE_TODO':
			return state.map(todo =>
				(todo.id === action.id)
				? {...todo, status: !todo.status}
				: todo
			)
		default:
			return state
	}
}

export default combineReducers({
	todos
});