import React from 'react';
import { toggleTodo } from '../actions';
import { connect } from 'react-redux';

function TodoList({ todos, toggleTodo }) {
	return (
		<ul>
			{todos.map(todo => 
				<li key={todo.id} onClick={() => toggleTodo(todo.id)}>
					<span>
						{todo.kegiatan}
					</span> - 
					<span>
						{todo.status ? 'sudah' : 'belum'}
					</span>
				</li>
			)}
		</ul>
	)
}

const mapStateToProps = state => ({
	todos: state.todos
})

const mapDispatchToProps = dispatch => ({
	toggleTodo: id => dispatch(toggleTodo(id))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TodoList);