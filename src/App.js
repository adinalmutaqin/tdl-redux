import React from 'react';
import AddTodo from './containers/AddTodo';
import TodoList from './containers/TodoList';

function App() {
  return (
    <div className="App">
      <AddTodo />
      <TodoList />
    </div>
  );
}

export default App;
